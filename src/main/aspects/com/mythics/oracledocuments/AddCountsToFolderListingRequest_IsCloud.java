package com.mythics.oracledocuments;

import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Jonathan Hult
 *
 */
@Aspect
public class AddCountsToFolderListingRequest_IsCloud {
	final static Logger log = LoggerFactory.getLogger(AddCountsToFolderListingRequest_IsCloud.class);

	@Pointcut("withincode(void addCountsToFolderListingRequest(..))")
	void addCountsToFolderListingRequest() {}
		
	@Pointcut("call(boolean isCloud())")
	void isCloud(){}
	 
	@Around("addCountsToFolderListingRequest() && isCloud()")
	public Boolean around() {
		log.trace("Overrode isCloud() to return false so addCountsToFolderListingRequest(..) can properly execute");
		return false;
	}
} 
