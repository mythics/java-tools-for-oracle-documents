package com.mythics.oracledocuments;
 
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.ProceedingJoinPoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Jonathan Hult
 * This class cannot use lombok since it is compiled with AJC.
 *
 */
@Aspect
public class UpdateFileContentInternal_ValidateRevision {
	final static Logger log = LoggerFactory.getLogger(UpdateFileContentInternal_ValidateRevision.class);
	
	@Pointcut("withincode(oracle.webcenter.sync.data.File updateFileContentInternal(..))")
	void updateFileContent() {}
	
	@Pointcut("call(String validateRevision(..)) && args(revisionId, allowAliases)")
	void validateRevision(String revisionId, boolean allowAliases) {}
	
	@Around("updateFileContent() && validateRevision(revisionId, allowAliases)")
	public String around(ProceedingJoinPoint thisJoinPoint, String revisionId, boolean allowAliases) throws Throwable {
		log.trace("Overrode validateRevision() so Cloud @latest can be used");
		return (String) thisJoinPoint.proceed(new Object[]{revisionId, true});
	}
} 
