package com.mythics.oracledocuments;

import static com.mythics.oracledocuments.MessageStrings.FILE_MISSING;
import static com.mythics.oracledocuments.MessageStrings.FOLDER_MISSING;
import static com.mythics.oracledocuments.MessageStrings.IS_MISSING;
import static lombok.AccessLevel.PRIVATE;
import static oracle.webcenter.sync.impl.FrameworkFoldersFields.fFileName;
import static oracle.webcenter.sync.impl.FrameworkFoldersFields.fFolderName;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

import lombok.NonNull;
import lombok.val;
import lombok.experimental.FieldDefaults;
import oracle.webcenter.sync.client.FolderBrowseRequest;
import oracle.webcenter.sync.data.ConflictResolutionMethod;
import oracle.webcenter.sync.data.File;
import oracle.webcenter.sync.data.Folder;
import oracle.webcenter.sync.data.Item;
import oracle.webcenter.sync.data.ItemList;

/**
 *
 * @author Jonathan Hult
 *
 */
@FieldDefaults(makeFinal = true, level = PRIVATE)
public class FolderTools {

	private static final String NO_MATCHING_FILES = "No matching items were found.";
	private static final String TOO_MANY_MATCHES = "Too many matches were found. This should not be possible since file names are supposed to be unique per folder.";

	DocsConnection odc;

	// Do not let anyone instantiate except package
	FolderTools(final DocsConnection odc) {
		this.odc = odc;
	}

	/**
	 * Get list of items in folder for a specified fbr.
	 *
	 * @param fbr
	 * @return ItemList containing folder items
	 */
	public ItemList getItems(@NonNull final FolderBrowseRequest fbr) {
		return odc.getItemService().getFolderItems(fbr);
	}

	/**
	 * Get list of items in folder for a specified folder.
	 *
	 * @param folder
	 * @return ItemList containing folder items
	 */
	public ItemList getItems(final Folder folder) {
		// does validation of parameters
		return getItems(DocsUtils.getFolderBrowseRequest(folder, null));
	}

	/**
	 * Get list of files in folder for a specified fbr.
	 *
	 * @param fbr
	 * @return ItemList containing files in folder
	 */
	public ItemList getFilesAsItemList(@NonNull final FolderBrowseRequest fbr) {
		DocsUtils.addItemType(fbr, ItemType.FILE);
		return getItems(fbr);
	}

	/**
	 * Get list of Items (all Files) in folder for a specified folder.
	 *
	 * @param folder
	 * @return ItemList containing files in folder
	 */
	public ItemList getFilesAsItemList(final Folder folder) {
		// does validation of parameters
		return getFilesAsItemList(DocsUtils.getFolderBrowseRequest(folder));
	}

	/**
	 * Get list of Files in folder for a specified fbr.
	 *
	 * @param fbr
	 * @return List containing Files in folder
	 */
	public List<File> getFilesAsFileList(final FolderBrowseRequest fbr) {
		// does validation of parameters
		return File.getFiles(getFilesAsItemList(fbr).getItems());
	}

	/**
	 * Get list of Files in folder for a specified folder.
	 *
	 * @param folder
	 * @return List containing Files in folder
	 */
	public List<File> getFilesAsFileList(final Folder folder) {
		// does validation of parameters
		return File.getFiles(getFilesAsItemList(folder).getItems());
	}

	/**
	 * Get list of Folders in folder for a specified fbr.
	 *
	 * @param fbr
	 * @return ItemList containing files in folder
	 */
	public ItemList getFoldersAsItemList(@NonNull final FolderBrowseRequest fbr) {
		DocsUtils.addItemType(fbr, ItemType.FOLDER);
		return getItems(fbr);
	}

	/**
	 * Get list of Items (all folders) in folder for a specified folder.
	 *
	 * @param folder
	 * @return ItemList containing files in folder
	 */
	public ItemList getFoldersAsItemList(final Folder folder) {
		// does validation of parameters
		return getFoldersAsItemList(DocsUtils.getFolderBrowseRequest(folder));
	}

	/**
	 * Get list of Folders in folder for a specified fbr.
	 *
	 * @param fbr
	 * @return List containing Files in folder
	 */
	public List<Folder> getFoldersAsFolderList(final FolderBrowseRequest fbr) {
		// does validation of parameters
		return DocsUtils.getFolders(getFoldersAsItemList(fbr).getItems());
	}

	/**
	 * Get list of Folders in folder for a specified folder.
	 *
	 * @param folder
	 * @return List containing Files in folder
	 */
	public List<Folder> getFoldersAsFolderList(final Folder folder) {
		// does validation of parameters
		return DocsUtils.getFolders(getFoldersAsItemList(folder).getItems());
	}

	/**
	 * Get folder for a specified folder and folderName.
	 *
	 * @param folder
	 * @param folderName
	 * @return Folder
	 */
	public Folder findFolderByName(final Folder folder, final String folderName) {
		// this method does validation of parameters
		val match = findItemByName(folder, folderName, ItemType.FOLDER);

		if (match.isFolder()) {
			return (Folder) match;
		}
		// defensive programming
		throw new IllegalStateException("Item is not a folder but should be");
	}

	/**
	 * Get item for a specified folder and name. Will only ever return 1 result (or an error if 0 or 2+).
	 *
	 * @param folder The parent folder to look in.
	 * @param name Name of the file or folder.
	 * @param type Either File or Folder.
	 * @return Item
	 */
	protected Item findItemByName(final Folder folder, @NonNull final String name, @NonNull final ItemType type) {
		ObjectValidator.validateItemParam(folder, FOLDER_MISSING);

		// Only find items of specified type
		val fbr = DocsUtils.getFolderBrowseRequest(folder, type);

		System.out.println("ItemType: " + type);
		if (type.equals(ItemType.FILE)) {
			fbr.filter(fFileName, fFileName);
		} else if (type.equals(ItemType.FOLDER)) {
			fbr.filter(fFolderName, fFolderName);
		} else {
			throw new IllegalArgumentException("Invalid ItemType: " + type);
		}

		System.out.println("name: " + name);

		try {
			fbr.filter("name", URLDecoder.decode(name, StandardCharsets.UTF_8.name()));

		} catch (final UnsupportedEncodingException e) {
			System.err.println("Could not properly decode file name: + " + name);
			e.printStackTrace(System.err);
		}

		// TODO: pagination - right now we assume only 1 page of results

		val itemList = getItems(fbr);
		val items = itemList.getItems();
		System.out.println("NumItems: " + items.size() + "    " + items);

		if ((items == null) || (items.size() == 0)) {
			throw new IllegalStateException(NO_MATCHING_FILES);
		} else if (items.size() > 1) {
			// defensive programming; should not happen; folder name should be
			// unique per folder
			throw new IllegalStateException(TOO_MANY_MATCHES);
		} else {
			val match = items.get(0);
			return match;
		}
	}

	/**
	 * Get list of folder paths for a specified file.
	 *
	 * @param file
	 * @return List of folders
	 */
	public List<Folder> getFolderPaths(final File file) {
		ObjectValidator.validateItemParam(file, FILE_MISSING);

		return odc.getItemService().getFolderPaths(file.getId(), false);
	}

	/**
	 * Get pretty folder path as String for a specified fFileGUID.
	 *
	 * @param fFileGUID
	 * @return pretty folder path
	 */
	public String getFolderPathsPretty(final File file) {
		val folders = getFolderPaths(file);
		return DocsUtils.getFolderPathsPretty(folders);
	}

	/**
	 * Delete a folder.
	 *
	 * @param folder
	 */
	public void deleteFolder(final Folder folder) {
		ObjectValidator.validateItemParam(folder, FOLDER_MISSING);

		odc.getSyncClient().getTrashService().moveItemToTrash(folder.getId());
	}

	/**
	 * Create a folder.
	 *
	 * @param folder
	 * @param crm
	 */
	public void createFolder(final Folder folder, final ConflictResolutionMethod crm) {
		ObjectValidator.validateItemParam(folder, FOLDER_MISSING);

		odc.getItemService().createFolder(folder, crm);
	}

	/**
	 * Move a folder.
	 *
	 * @param source
	 * @param target
	 * @param crm
	 */
	public void moveFolder(final Folder source, final Folder target, final ConflictResolutionMethod crm) {
		ObjectValidator.validateItemParam(source, "source" + IS_MISSING);
		ObjectValidator.validateItemParam(target, "target" + IS_MISSING);

		odc.getItemService().moveFolder(source.getId(), target.getParentId(), crm);
	}
}