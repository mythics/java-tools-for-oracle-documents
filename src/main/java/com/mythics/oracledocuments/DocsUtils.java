package com.mythics.oracledocuments;

import static com.mythics.oracledocuments.MessageStrings.FOLDER_MISSING;
import static lombok.AccessLevel.PRIVATE;
import static oracle.webcenter.sync.client.FolderAlias.CLOUD_HOME;
import static org.apache.commons.lang.StringUtils.isEmpty;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import lombok.NonNull;
import lombok.val;
import lombok.experimental.FieldDefaults;
import lombok.experimental.UtilityClass;
import oracle.stellent.ridc.IdcClientException;
import oracle.stellent.ridc.model.DataBinder;
import oracle.webcenter.sync.client.FolderBrowseRequest;
import oracle.webcenter.sync.data.File;
import oracle.webcenter.sync.data.FileContent;
import oracle.webcenter.sync.data.Folder;
import oracle.webcenter.sync.data.Item;
import oracle.webcenter.sync.exception.SyncException;
import oracle.webcenter.sync.impl.IdMapper;
import oracle.webcenter.sync.impl.IdcDataObjectWrapper;
import oracle.webcenter.sync.impl.IdcRequestBuilder;

@FieldDefaults(makeFinal = true, level = PRIVATE)
@UtilityClass
public class DocsUtils {

	String HOME = "Home";
	String CONTENT_TYPE_STREAM = "application/octet-stream";

	/**
	 * Get a local File object that holds the fFileGUID.
	 *
	 * @param fFileGUID
	 * @return File object
	 */
	public File getLocalFileByGUID(final String fFileGUID) {
		ObjectValidator.validateFileIdNotEmpty(fFileGUID);

		val file = new File();
		file.setId(addFilePrefix(fFileGUID));
		return file;
	}

	/**
	 * Get a Folder object that holds the fFolderGUID.
	 *
	 * @param fFolderGUID
	 * @return Folder
	 */
	public Folder getLocalFolderByGUID(final String fFolderGUID) {
		ObjectValidator.validateFolderIdNotEmpty(fFolderGUID);

		val folder = new Folder();
		folder.setId(addFolderPrefix(fFolderGUID));
		return folder;
	}

	/**
	 * Get a local Folder object that holds the fFolderName.
	 *
	 * @param fFolderName
	 * @return Folder
	 */
	public Folder getLocalFolderByName(@NonNull final String fFolderName) {
		val folder = new Folder();
		folder.setName(fFolderName);
		return folder;
	}

	/**
	 * Create local FileContent object.
	 *
	 * @param filePath
	 * @param fileName Defaults to name from file
	 * @return FileContent
	 * @throws FileNotFoundException
	 */
	public FileContent getLocalFileContent(@NonNull final Path filePath, final String fileName) throws FileNotFoundException {
		val file = filePath.toFile();
		val fileNameToUse = isEmpty(fileName) ? file.getName() : fileName;
		return new FileContent(new FileInputStream(file), CONTENT_TYPE_STREAM, file.length(), fileNameToUse);
	}

	/**
	 * Delete local path with no exceptions thrown even if file path is null or does not exist.
	 *
	 * @param filePath
	 */
	public void deleteLocalPathQuietly(final Path filePath) {
		try {
			if (filePath != null) {
				// Remove local file
				Files.delete(filePath);
			}
		} catch (final Exception e) {
			e.printStackTrace(System.err);
			// No need to do anything but log
		}
	}

	/**
	 * Add fFileGUID: prefix if it is missing.
	 *
	 * @param fFileGUID
	 * @return fFileGUID with proper prefix
	 */
	public String addFilePrefix(final String fFileGUID) {
		ObjectValidator.validateFileIdNotEmpty(fFileGUID);

		return IdMapper.isFileId(fFileGUID) ? fFileGUID : IdMapper.idFromFileGUID(fFileGUID);
	}

	/**
	 * Add fFolderGUID prefix if it is missing.
	 *
	 * @param fFolderGUID
	 * @return fFolderGUID with proper prefix
	 */
	public String addFolderPrefix(final String fFolderGUID) {
		ObjectValidator.validateFolderIdNotEmpty(fFolderGUID);

		return IdMapper.isFolderId(fFolderGUID) ? fFolderGUID : IdMapper.idFromFolderGUID(fFolderGUID);
	}

	public String removeFilePrefix(final String fFileGUID) {
		ObjectValidator.validateFileIdNotEmpty(fFileGUID);

		return IdMapper.isFileId(fFileGUID) ? fFileGUID.substring(IdMapper.FILE_ID_PREFIX.length()) : fFileGUID;
	}

	public String removeFolderPrefix(final String fFolderGUID) {
		ObjectValidator.validateFileIdNotEmpty(fFolderGUID);

		return IdMapper.isFolderId(fFolderGUID) ? fFolderGUID.substring(IdMapper.FOLDER_ID_PREFIX.length()) : fFolderGUID;
	}

	/**
	 * Add file or folder browse type. If itemType is null, both files and folders will be included.
	 *
	 * @param fbr
	 * @param itemType
	 */
	public void addItemType(@NonNull final FolderBrowseRequest fbr, @NonNull final ItemType type) {
		fbr.type(type.getValue());
	}

	/**
	 * Get FolderBrowseRequest with both files and folders.
	 *
	 * @param folder
	 * @return FolderBrowseRequest for items in folder
	 */
	public FolderBrowseRequest getFolderBrowseRequest(final Folder folder) {
		// does validation of parameters
		return getFolderBrowseRequest(folder, null);
	}

	/**
	 * Get FolderBrowseRequest with either files or folders for a specified folder. If itemType is null, both files and folders will be included. Limit is 10,000 items (combined
	 * between files and folders).
	 *
	 * @param folder
	 * @param type file or folder
	 * @return FolderBrowseRequest for items in folder
	 */
	public FolderBrowseRequest getFolderBrowseRequest(final Folder folder, final ItemType type) {
		ObjectValidator.validateItemParam(folder, FOLDER_MISSING);

		// type not required

		val fbr = FolderBrowseRequest.browse(folder.getId());
		if (type != null) {
			DocsUtils.addItemType(fbr, type);
		}
		fbr.pagination(0, 10000);
		return fbr;
	}

	// TODO: Fix Javadoc summary
	/**
	 * Similar to {@link java.nio.file.Files#.getFiles()}.
	 *
	 *
	 * @param items Collection of Folders as Items. This can be null which will return an empty ArrayList.
	 * @return Folder objects in a List.
	 */
	public List<Folder> getFolders(final Collection<Item> items) {
		val size = items == null ? 0 : items.size();
		val folders = new ArrayList<Folder>(size);
		if (items != null) {
			for (val item : items) {
				if (item.isFolder()) {
					folders.add((Folder) item);
				}
			}
		}
		return folders;
	}

	/**
	 * Get pretty folder path as String for a specified fFileGUID.
	 *
	 * @param folders
	 * @return pretty folder path
	 */
	public String getFolderPathsPretty(@NonNull final List<Folder> folders) {
		val sb = new StringBuilder();

		val iterator = folders.listIterator();
		while (iterator.hasNext()) {
			String name = iterator.next().getName();
			if (name.equals(CLOUD_HOME)) {
				name = HOME;
			}
			sb.append(name);
			if (iterator.hasNext()) {
				sb.append(" > ");
			}
		}
		return sb.toString();
	}

	/**
	 * Get DataBinder wrapped in an IdcDataObjectWrapper.
	 *
	 * @param binder
	 * @return IdcDataObjectWrapper wrapping the binder
	 */
	public IdcDataObjectWrapper getBinderAsWrapper(@NonNull final DataBinder binder) {
		val responseWrapper = new IdcDataObjectWrapper(binder);
		return responseWrapper;
	}

	/**
	 * Execute an IdcService.
	 *
	 * @param serviceName
	 * @return IdcDataObjectWrapper from response
	 */
	public DataBinder executeService(@NonNull final IdcRequestBuilder request) {
		try {
			// Execute
			val response = request.executeSimple();
			return response;
		} catch (final IdcClientException e) {
			throw new SyncException("Error executing the service", e);
		}
	}
}
