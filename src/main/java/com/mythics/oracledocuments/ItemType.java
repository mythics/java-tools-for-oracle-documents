package com.mythics.oracledocuments;

import static lombok.AccessLevel.PRIVATE;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import oracle.webcenter.sync.data.Item;

/**
 *
 * @author Jonathan Hult
 *
 */
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
public enum ItemType {
	FOLDER(Item.ITEM_TYPE_FOLDER),
	FILE(Item.ITEM_TYPE_FILE);

	@Getter
	String value;
}
