package com.mythics.oracledocuments;

import static com.mythics.oracledocuments.MessageStrings.FILE_MISSING;
import static com.mythics.oracledocuments.MessageStrings.FOLDER_MISSING;
import static lombok.AccessLevel.PRIVATE;

import java.io.FileNotFoundException;
import java.nio.file.Path;

import lombok.NonNull;
import lombok.val;
import lombok.experimental.FieldDefaults;
import oracle.stellent.ridc.model.DataBinder;
import oracle.stellent.ridc.model.DataObject;
import oracle.webcenter.sync.client.CheckinRequest;
import oracle.webcenter.sync.client.SyncClient;
import oracle.webcenter.sync.data.ConflictResolutionMethod;
import oracle.webcenter.sync.data.File;
import oracle.webcenter.sync.data.FileContent;
import oracle.webcenter.sync.data.Folder;
import oracle.webcenter.sync.impl.BaseService;
import oracle.webcenter.sync.impl.IdcRequestBuilder;
import oracle.webcenter.sync.impl.SyncClientImpl;

/**
 *
 * @author Jonathan Hult
 *
 */
@FieldDefaults(makeFinal = true, level = PRIVATE)
public class ServiceTools extends BaseService {

	private ServiceTools() {
		super(null);
	}

	protected ServiceTools(@NonNull final SyncClient syncClient) {
		super((SyncClientImpl) syncClient);
	}

	/**
	 * Checkin a new revision.
	 *
	 * @param filePath
	 * @param file
	 * @param fileName Defaults to the file name from filePath.
	 * @param crm Defaults to FailDuplicates.
	 * @return File
	 * @throws FileNotFoundException
	 */
	public File updateFile(@NonNull final Path filePath, @NonNull final File file, final String fileName, final ConflictResolutionMethod crm) throws FileNotFoundException {
		ObjectValidator.validateItemParam(file, FILE_MISSING);
		val request = CheckinRequest.checkinRev(file.getId(), null);
		return doCheckin(filePath, fileName, request, crm);
	}

	/**
	 * Checkin a file into a folder.
	 *
	 * @param filePath
	 * @param folder
	 * @param fileName Defaults to the file name from filePath.
	 * @param crm Defaults to FailDuplicates.
	 * @return File
	 * @throws FileNotFoundException
	 */
	public File checkin(@NonNull final Path filePath, final Folder folder, @NonNull final String fileName, final ConflictResolutionMethod crm) throws FileNotFoundException {
		ObjectValidator.validateItemParam(folder, FOLDER_MISSING);
		val request = CheckinRequest.checkinNew();
		request.parentId(folder.getId());
		return doCheckin(filePath, fileName, request, crm);
	}

	/**
	 * Internal method which either checks in a new revision or updates an existing revision.
	 *
	 * @param filePath
	 * @param fileName Defaults to the file name from filePath.
	 * @param request
	 * @param crm Defaults to FailDuplicates.
	 * @return
	 */
	private File doCheckin(@NonNull final Path filePath, final String fileName, final CheckinRequest request, final ConflictResolutionMethod crm)
	        throws FileNotFoundException {

		if (crm == null) {
			request.conflictResolutionMethod(ConflictResolutionMethod.FailDuplicates);
		} else {
			request.conflictResolutionMethod(crm);
		}

		File fileToReturn = null;
		try (FileContent fc = DocsUtils.getLocalFileContent(filePath, fileName)) {
			request.content(fc);
			if (request.parentId == null) {
				fileToReturn = syncClient.getItemsService().createFile(request);
			} else {
				fileToReturn = syncClient.getItemsService().updateFileContent(request);
			}
		}
		return fileToReturn;
	}

	/**
	 * Get IdcRequestBuilder for GET service.
	 *
	 * @param serviceName
	 * @return IdcRequestBuilder for GET Service
	 */
	public IdcRequestBuilder getRequestBuilderGetService(final String serviceName) {
		return idcGET(serviceName);
	}

	/**
	 * Execute service as GET request (for services that do not change data).
	 *
	 * @param serviceName
	 * @param params - all parameters will be included
	 * @return IdcDataObjectWrapper from response
	 */
	public DataBinder executeGetService(final String serviceName, final DataObject params) {
		return executeGetService(serviceName, params, (String[]) null);
	}

	/**
	 * Execute service as GET request (for services that do not change data).
	 *
	 * @param serviceName
	 * @param params
	 * @param paramNames - those parameters to include
	 * @return IdcDataObjectWrapper from response
	 */
	public DataBinder executeGetService(@NonNull final String serviceName, final DataObject params, final String... paramNames) {
		val request = idcGET(serviceName);
		return executeService(request, params, paramNames);
	}

	/**
	 * Get IdcRequestBuilder for POST service.
	 *
	 * @param serviceName
	 * @return IdcRequestBuilder for POST Service
	 */
	public IdcRequestBuilder getRequestBuilderPostService(@NonNull final String serviceName) {
		return idcPOST(serviceName);
	}

	/**
	 * Execute service as POST request (for services that changes data).
	 *
	 * @param serviceName
	 * @param params - all parameters will be included
	 * @return IdcDataObjectWrapper from response
	 */
	public DataBinder executePostService(@NonNull final String serviceName, final DataObject params) {
		return executePostService(serviceName, params, (String[]) null);
	}

	/**
	 * Execute service as POST request (for services that changes data).
	 *
	 * @param serviceName
	 * @param params
	 * @param paramNames - those parameters to include
	 * @return DataBinder from response
	 */
	public DataBinder executePostService(@NonNull final String serviceName, final DataObject params, final String... paramNames) {
		val request = getRequestBuilderPostService(serviceName);
		return executeService(request, params, paramNames);
	}

	/**
	 * Execute an IdcService.
	 *
	 * @param serviceName
	 * @param params
	 * @param paramNames
	 * @return IdcDataObjectWrapper from response
	 */
	public static DataBinder executeService(@NonNull final IdcRequestBuilder request, final DataObject params, final String... paramNames) {
		if ((params != null) && !params.isEmpty()) {
			String[] paramNamesToCopy = paramNames;
			// Copy all params if paramNames is empty
			if ((paramNames == null) || (paramNames.length == 0)) {
				paramNamesToCopy = params.keySet().toArray(new String[0]);
			}

			// Copy params
			request.copyParams(params, paramNamesToCopy);
		}

		// Execute
		return DocsUtils.executeService(request);
	}
}
