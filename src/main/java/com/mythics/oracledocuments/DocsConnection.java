package com.mythics.oracledocuments;


import java.net.URI;

import org.aspectj.weaver.loadtime.Agent;

import static lombok.AccessLevel.PRIVATE;

import lombok.NonNull;
import lombok.Value;
import lombok.val;
import lombok.experimental.FieldDefaults;
import oracle.webcenter.sync.client.ItemsService;
import oracle.webcenter.sync.client.SyncClient;
import oracle.webcenter.sync.client.SyncClientFactory;

/**
 *
 * This class creates a connection to Oracle Documents Cloud Service.
 *
 * @author Jonathan Hult 
 * 
 */
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Value
public class DocsConnection {
	// Oracle Documents
	public final static String DOCUMENTS_DESKTOP = "/documents/desktop";
	
	@NonNull
	SyncClient syncClient;
	@NonNull
	ItemsService itemService;
	@NonNull
	FileTools fileTools;
	@NonNull
	FolderTools folderTools;
	@NonNull
	ServiceTools serviceTools;

	/**
	 * Connection to Oracle Documents Cloud Service.
	 *
	 * @param username
	 * @param password
	 * @param server 
	 */ 
	public DocsConnection(@NonNull final String username, @NonNull final char[] password, @NonNull final String server) {
		checkAspectJAgentLoaded();
		
		val serverURI = URI.create(server + DOCUMENTS_DESKTOP);
		syncClient = SyncClientFactory.instance().createSyncClient(serverURI, username, password);
		checkConnection();
		
		itemService = syncClient.getItemsService();
		fileTools = new FileTools(this);
		folderTools = new FolderTools(this);
		serviceTools = new ServiceTools(syncClient);
	}

	/**
	 * Check if AspectJ loadtime weaver agent is loaded.
	 *
	 * @throws UnsupportedOperationException
	 */
	public static void checkAspectJAgentLoaded() {
		Agent.getInstrumentation();
	}

	/**
	 * Ping the Oracle Documents server.
	 */
	public void checkConnection() {
		syncClient.refreshServerInfo();
	}
}
