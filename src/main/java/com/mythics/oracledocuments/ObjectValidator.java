package com.mythics.oracledocuments;

import static com.mythics.oracledocuments.MessageStrings.IS_MISSING;
import static org.apache.commons.lang.StringUtils.isBlank;

import lombok.NonNull;
import lombok.experimental.UtilityClass;
import oracle.webcenter.sync.data.Item;
import oracle.webcenter.sync.exception.InvalidIdException;
import oracle.webcenter.sync.impl.FrameworkFoldersFields;

/**
 *
 * @author Jonathan Hult
 *
 */
@UtilityClass
public class ObjectValidator {

	private final String FILE_ID_MISSING = FrameworkFoldersFields.fFileGUID + IS_MISSING;
	private final String FOLDER_ID_MISSING = FrameworkFoldersFields.fFolderGUID + IS_MISSING;

	/**
	 * Verifies that there is a valid ID in the Item.
	 *
	 * @param item
	 * @param errMsg
	 * @throws IllegalArgumentException
	 */
	public void validateItemParam(@NonNull final Item item, final String errMsg) {
		validateIdNotEmpty(item.getId(), errMsg);
	}

	public void validateFileIdNotEmpty(final String fFileGUID) {
		validateIdNotEmpty(fFileGUID, FILE_ID_MISSING);
	}

	public void validateFolderIdNotEmpty(final String fFolderGUID) {
		validateIdNotEmpty(fFolderGUID, FOLDER_ID_MISSING);
	}

	private void validateIdNotEmpty(final String id, final String errMsg) {
		if (isBlank(id)) {
			throw new InvalidIdException(errMsg, null);
		}
	}
}
