package com.mythics.oracledocuments;

/**
 *
 * @author Jonathan Hult
 *
 */

public class MessageStrings {

	private MessageStrings() {
	}

	// Error messages
	public static final String IS_MISSING = " parameter is missing or is empty.";
	public static final String FOLDER_MISSING = "folder" + IS_MISSING;
	public static final String FILE_MISSING = "file" + IS_MISSING;
}
