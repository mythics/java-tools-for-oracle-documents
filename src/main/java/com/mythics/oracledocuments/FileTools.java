package com.mythics.oracledocuments;

import static com.mythics.oracledocuments.MessageStrings.FILE_MISSING;
import static com.mythics.oracledocuments.MessageStrings.FOLDER_MISSING;
import static com.mythics.oracledocuments.MessageStrings.IS_MISSING;
import static lombok.AccessLevel.PRIVATE;
import static oracle.webcenter.sync.client.RevisionAlias.LATEST_RELEASED;
import static oracle.webcenter.sync.data.ConflictResolutionMethod.FailDuplicates;
import static oracle.webcenter.sync.data.ConflictResolutionMethod.ReviseDuplicates;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import lombok.NonNull;
import lombok.val;
import lombok.experimental.FieldDefaults;
import oracle.webcenter.sync.client.CheckinRequest;
import oracle.webcenter.sync.data.ConflictResolutionMethod;
import oracle.webcenter.sync.data.File;
import oracle.webcenter.sync.data.FileContent;
import oracle.webcenter.sync.data.Folder;

/**
 *
 * @author Jonathan Hult
 *
 */
@FieldDefaults(makeFinal = true, level = PRIVATE)
public class FileTools {

	DocsConnection odc;

	FileTools(final DocsConnection odc) {
		this.odc = odc;
	}

	/**
	 * Get file for a specified fFileGUID.
	 *
	 * @param fFileGUID
	 * @return File
	 */
	public File getRemoteFileByGUID(final String fFileGUID) {
		// This does the validation of the fFileGUID
		val localFile = DocsUtils.getLocalFileByGUID(fFileGUID);

		return odc.getItemService().getFile(localFile.getId());
	}

	/**
	 * Get file for a specified folder and fileName.
	 *
	 * @param folder
	 * @param fileName
	 * @return File
	 */
	public File findFileByName(final Folder folder, @NonNull final String fileName) {
		ObjectValidator.validateItemParam(folder, FOLDER_MISSING);

		val match = odc.getFolderTools().findItemByName(folder, fileName, ItemType.FILE);

		if (match.isFile()) {
			return (File) match;
		}
		// defensive programming
		throw new IllegalStateException("Item is not a file but should be");
	}

	/**
	 * Download file for a specified fFileGUID. remoteFileContent will be used if not null.
	 *
	 * @param localFile
	 * @param remoteFileContent
	 * @param downloadPath
	 * @throws IOException
	 */
	public void downloadFile(final File localFile, final FileContent remoteFileContent, @NonNull final Path downloadPath) throws IOException {
		ObjectValidator.validateItemParam(localFile, "localFile" + IS_MISSING);

		val actualRemoteFileContent = remoteFileContent == null ? getRemoteFileContent(localFile) : remoteFileContent;
		Files.write(downloadPath, actualRemoteFileContent.getBytes());
		FileContent.closeQuietly(actualRemoteFileContent);
	}

	/**
	 * Download file for a specified fileName in a specified folder.
	 *
	 * @param folder
	 * @param fileName
	 * @param downloadPath
	 * @throws IOException
	 */
	public void downloadFileByName(final Folder folder, @NonNull final String fileName, @NonNull final Path downloadPath) throws IOException {
		ObjectValidator.validateItemParam(folder, FOLDER_MISSING);

		// Find fFileGUID for specified fileName in folderID
		val fileToDownload = findFileByName(folder, fileName);
		downloadFile(fileToDownload, null, downloadPath);
	}

	/**
	 * Get FileContent for a specified file. Defaults to LatestReleased
	 *
	 * @param localFile
	 * @return FileContent
	 */
	public FileContent getRemoteFileContent(final File localFile) {
		ObjectValidator.validateItemParam(localFile, "localFile" + IS_MISSING);

		return odc.getItemService().getFileContent(localFile.getId(), LATEST_RELEASED);
	}

	/**
	 * Checkin a new revision of a file.
	 *
	 * @param localFile
	 * @param content FileContent containing new file revision's content
	 * @return File
	 */
	public File uploadNewRevision(final File localFile, final FileContent content) {
		val req = CheckinRequest.checkinRev(localFile);
		req.conflictResolutionMethod(ReviseDuplicates);

		return uploadFile(localFile, content, req);
	}

	/**
	 * Checkin a new file.
	 *
	 * @param localFile
	 * @param content FileContent containing new file's content
	 * @return File
	 */
	public File uploadNewFile(final File localFile, final FileContent content) {
		val req = CheckinRequest.checkinNew();
		req.conflictResolutionMethod(FailDuplicates);

		return uploadFile(localFile, content, req);
	}

	/**
	 * Checkin a new file or a revision.
	 *
	 * @param localFile
	 * @param content FileContent containing new file's content
	 * @return File
	 */
	private File uploadFile(final File localFile, @NonNull final FileContent content, final CheckinRequest req) {
		ObjectValidator.validateItemParam(localFile, "localFile" + IS_MISSING);

		req.parentId(localFile.getParentId());
		req.content(content);

		return odc.getItemService().createFile(req);
	}

	/**
	 * Reserve a file.
	 *
	 * @param file
	 *
	 * @return File
	 */
	public File reserveFile(final File file) {
		ObjectValidator.validateItemParam(file, FILE_MISSING);

		return odc.getItemService().setReservation(file.getId(), null);
	}

	/**
	 * Clear a reservation.
	 *
	 * @param file
	 *
	 * @return File
	 */
	public File clearReservation(final File file) {
		ObjectValidator.validateItemParam(file, FILE_MISSING);

		return odc.getItemService().clearReservation(file.getId(), file.getReservedBy().getId(), null);
	}

	/**
	 * Move a file.
	 *
	 * @param source
	 * @param target
	 * @param crm
	 *
	 * @return File
	 */
	public File moveFile(final File source, final Folder target, final ConflictResolutionMethod crm) {
		ObjectValidator.validateItemParam(source, "source" + IS_MISSING);
		ObjectValidator.validateItemParam(target, "target" + IS_MISSING);

		return odc.getItemService().moveFile(source.getId(), target.getId(), crm);
	}

	/**
	 * Delete a file.
	 *
	 * @param file
	 */
	public void deleteFile(final File file) {
		ObjectValidator.validateItemParam(file, FILE_MISSING);

		odc.getSyncClient().getTrashService().moveItemToTrash(file.getId());
	}
}
