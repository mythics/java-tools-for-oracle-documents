# Java Tools for Oracle Documents

## Information
* Created by: Mythics
* Author: Jonathan Hult
* Version: 0.4

## Overview
This repository contains the source for a (Maven) JAR file which can be used to interact with [Oracle Documents Cloud Service](https://jonathanhult.com/blog/2015/02/oracle-documents-cloud-service-roundup/).

## Maven
### Repo
```
<repositories>
	<repository>
		<id>Bintray - jhult</id>
		<url>https://dl.bintray.com/jhult/maven/</url>
	</repository>
</repositories>
```
### Dependency snippet
```
<dependency>
	  <groupId>com.mythics</groupId>
	  <artifactId>oracledocuments</artifactId>
	  <version>0.4</version>
</dependency>
```

## Requirements
This component currently requires that AspectJ Load-time weaving (LTW) is used as the javaagent. More information on AspectJ LTW can be found [here](https://eclipse.org/aspectj/doc/released/devguide/ltw.html).

### Example
```
java -javaagent:lib/aspectjweaver.jar -jar mylibrary.jar
```